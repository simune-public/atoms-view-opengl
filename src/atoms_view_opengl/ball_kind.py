from ase.data import covalent_radii
from ase.data.colors import cpk_colors, jmol_colors
import numpy as np


class BallKind:
    """ The class describes the atom """

    def __init__(self, atomic_number: int, tag: int):
        """
        Args:
            atomic_number: atomic number (int)
            is_constrained: True if the atom is constrained (bool)
        """
        self.atomic_number: int = atomic_number
        # The number of a chemical element in the periodic table.
        alpha = 0.5 if tag == -atomic_number else 1.0
        # color type: 0 - cpk_colors; 1 - jmol_colors
        self.color_type = 0
        self.color = np.array((*cpk_colors[self.atomic_number], alpha), dtype='float32')
        delta_color = np.linalg.norm(self.color[0:3] - np.array([1, 0.2, 0]))
        self.contrast_color = np.array([255, 0, 0]) if delta_color > 0.5 else np.array([0, 0, 255])
        # Color of atom according to CPK coloring.
        self.radius: float = covalent_radii[self.atomic_number] / 2
        # Radius of atom. Factor 2 is empirical.
        self.num_phi: int = 30
        self.num_theta: int = 15
        # Data for fixed atoms representation
        width = self.radius / 8.0
        pos = 0.7 * self.radius
        self.fixed_vert = np.array([(-width, -pos), (-width, pos), (width, pos),
                          (width, -pos), (-width, -pos), (width, pos),
                          (-pos, -width), (-pos, width), (pos, width),
                          (pos, -width), (-pos, -width), (pos, width)], dtype='float32')

    @property
    def alpha(self) -> np.float32:
        return self.color[3]

    @property
    def needs_screen_rotation(self) -> bool:
        """
        This boolean I will use to determine whether the ball needs to be rotated to always
        show the same "side" to the screen projection plane.
        Returns:
            True if the ball needs to be rotated, False otherwise.
        """
        return self.alpha < 1.0

    def change_atomic_color(self):
        self.color_type = (self.color_type + 1) % 2
        self.color[0:3] = cpk_colors[self.atomic_number] if self.color_type == 0 else jmol_colors[self.atomic_number]

    def __repr__(self):
        return f'BallKind({self.atomic_number})'
