import numpy as np
import math


def compute_rotation_matrix_xyz(angles: np.array) -> np.ndarray:
    """
    Compose a rotation matrix composed of three consecutive rotations around x, y' and z''
    axes.
    Args:
        angles: np.array of alpha, beta, gamma, where
                alpha: angle of rotation around x-axis
                beta: angle of rotation around y' axis.
                gamma: angle of rotation around z'' axis.

    Returns:
        A 4 by 4 matrix of rotations.
    """
    alpha, beta, gamma = angles[0], angles[1], angles[2]

    c, s = math.cos(alpha), math.sin(alpha)
    m_x = np.array(((1, 0, 0), (0, c, -s), (0, s, c)), dtype='float32')

    c, s = math.cos(beta), math.sin(beta)
    m_y = np.array(((c, 0, s), (0, 1, 0), (-s, 0, c)), dtype='float32')

    c, s = math.cos(gamma), math.sin(gamma)
    m_z = np.array(((c, -s, 0), (s, c, 0), (0, 0, 1)), dtype='float32')

    rotation_matrix = np.eye(4, dtype='float32')
    rotation_matrix[:3, :3] = np.dot(m_z, m_y).dot(m_x)
    return rotation_matrix


def get_rotation_matrix_sph(vec: np.ndarray) -> np.ndarray:
    """
    Compute a rotation matrix for a couple of rotations to transform a vector along z axis
    to a vector along a given vector `vec`.
    Args:
        vec: the given (3D) vector

    Returns:
        The 4 by 4 transform matrix.
    """

    mat = np.eye(4, dtype='float32')

    xy_norm = np.linalg.norm(vec[0:2])
    if abs(xy_norm) < 1.0e-5:
        if vec[2] < 0.0:
            mat[2, 2] = -1.0
    else:
        xy_unit = vec[0:2] / xy_norm
        zr_vec = np.array((vec[2], xy_norm))
        zr_unit = zr_vec / np.linalg.norm(zr_vec)

        c, s, t, f = xy_unit[0], xy_unit[1], zr_unit[0], zr_unit[1]

        mat[0, :3] = t * c, t * s, -f
        mat[1, :2] = -s, c
        mat[2, :3] = f * c, f * s, t

    return mat
