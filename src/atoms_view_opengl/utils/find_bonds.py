from typing import List, Tuple
import numpy as np
import scipy.spatial

from ase.data import covalent_radii


def find_bonds_brute_force(atoms_pos: np.ndarray, atomic_numbers: np.ndarray) -> Tuple[np.ndarray,
                                                                                       np.ndarray]:
    """
    The function finds the pairs of atom indices which need a bond to be drawn between them.
    Args:
        atoms_pos: the result of get_atomic_numbers() of ASE Atoms.
        atomic_numbers: the result of ase_atoms.get_positions() of ASE Atoms.

    Returns:
        Tuple of two 1D arrays of the same size with atomic indices.
    """
    r_coval = covalent_radii[atomic_numbers]
    d_coval = r_coval + r_coval[:, None]
    d = scipy.spatial.distance.cdist(atoms_pos, atoms_pos)
    d -= d_coval / 0.7
    d_up = d - np.tril(d)
    return np.where(d_up < 0)


def find_bonds(positions: np.ndarray, zz: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    The function finds the pairs of atom indices which need a bond to be drawn between them.
    Args:
        positions: the result of get_positions() of ASE Atoms.
        zz: the result of ase_atoms.get_atomic_numbers() of ASE Atoms.

    Returns:
        Tuple of two 1D arrays of the same size with atomic indices.
    """
    species2atomic_number = np.unique(zz)
    r_cov = covalent_radii[species2atomic_number]
    d_cov_007 = (r_cov + r_cov[:, None]) / 0.7

    a2a = np.arange(len(zz), dtype=int)
    sp2tree, sp2aa = [], []
    for sp, z in enumerate(species2atomic_number):
        mask = zz == z
        sp2aa.append(a2a[mask])
        sp2tree.append(scipy.spatial.cKDTree(positions[mask]))

    group2aa, group2bb = [], []
    for sp1, (tree1, i2a) in enumerate(zip(sp2tree, sp2aa)):
        pairs_query = tree1.query_pairs(r=d_cov_007[sp1, sp1])
        if len(pairs_query) > 0:
            pair2ab = np.array(list(pairs_query), dtype=int).T
            group2aa.append(i2a[pair2ab[0]])
            group2bb.append(i2a[pair2ab[1]])

        start = sp1 + 1
        for sp2, (tree2, j2a) in enumerate(zip(sp2tree[start:], sp2aa[start:]), start=start):
            r_max = d_cov_007[sp1, sp2]
            pairs_lol = tree2.query_ball_tree(tree1, r=r_max)
            if np.fromiter((len(p) for p in pairs_lol), dtype=int).sum() > 0:
                pair2i = np.fromiter((i for j, ii in enumerate(pairs_lol) for i in ii), dtype=int)
                pair2j = np.fromiter((j for j, ii in enumerate(pairs_lol) for i in ii), dtype=int)

                group2aa.append(i2a[pair2i])
                group2bb.append(j2a[pair2j])

    if len(group2aa) > 0:
        bb, aa = np.concatenate(group2bb), np.concatenate(group2aa)
    else:
        bb, aa = np.zeros(0, dtype=int), np.zeros(0, dtype=int)
    return bb, aa
