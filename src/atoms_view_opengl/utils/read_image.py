from pathlib import Path
from PIL import Image as Image
import numpy as np


def read_image(file_name: str) -> np.ndarray:
    """
    Read an image (stored in the root source folder)
    Args:
        file_name: name of the file

    Returns:
        Numpy array of shape = (width, height, 4<RGBA>) and dtype=uint8
    """
    img = Image.open(Path(__file__).parent.parent / file_name)
    img_data = np.array(img)
    return img_data.reshape((*img.size, 4))
