from typing import Callable, Optional, Tuple, List

import OpenGL.GL as gl
import OpenGL.GLU as glu
import numpy as np

import math

from ase.data import covalent_radii

from atoms_view_opengl.ball_stick_model import BallStickModel
from atoms_view_opengl.utils.read_image import read_image
from atoms_view_opengl.utils.rotation_matrix import (compute_rotation_matrix_xyz,
                                                     get_rotation_matrix_sph)


class OpenGLCore:
    """ This class implements the opengl functionality """
    CLEAR_COLORS = ((0.0, 0.0, 0.0, 1.0), (0.2, 0.2, 0.2, 1.0),
                    (0.5, 0.5, 0.9, 1.0), (1.0, 1.0, 1.0, 1.0))

    NUM_LIST: int = 3
    ATOMS: int = 0
    BONDS: int = 1
    CELL: int = 2
    TEX_NUM: int = 1
    TEX_FONT_ID: int = 0

    PERSPECTIVE_ANGLE: float = 35.0

    def __init__(self, *, make_current_callback: Callable,
                 width_callback: Callable[[], int], height_callback: Callable[[], int],
                 clear_color: Tuple[float, float, float, float]):

        """
        Before any OpenGL interaction starts, we should make the relevant OpenGL context being
        "current". In PySide2/PyQt5, one normally uses the method makeCurrent(). However,
        we don't want the garbage collector to lock because of circular dependencies. Therefore,
        only the method QOpenGLWidget.makeCurrent() should be provided, not the widget object.

        Args:
            make_current_callback: pointer to QOpenGLWidget.makeCurrent()
            width_callback: pointer to QWidget.width()
            height_callback: pointer to QWidget.height()
            clear_color: color of the OpenGL canvas background (RGBA, 0.0-1.0 encoded).
        """

        self.make_current_callback = make_current_callback
        self.width_callback = width_callback
        self.height_callback = height_callback

        self.ball_stick_model: Optional[BallStickModel] = None  # Information about model

        self.atoms_first_handle: Optional[int] = None  # OpenGL handles for atoms and bonds

        self.species_first_handle: Optional[int] = None  # OpenGL handles for chemical species

        self.tex_ids = np.zeros(self.TEX_NUM, dtype='uint32')  # OpenGL handles for textures
        self.is_tex_loaded = False

        # model exist
        self.is_active: bool = False

        # camera position
        self.cam_position = np.array((0.0, 0.0, 10.0))

        # position of light
        self.light0_position = np.array((0.0, 0.0, 100.0, 1))

        self.xs_old: int = 0
        self.ys_old: int = 0

        self.perspective_angle = self.PERSPECTIVE_ANGLE

        self._rotation_angles: np.ndarray = np.zeros(3)  # angle of rotation
        self._rotation_matrix_xyz = np.eye(4, 4)  # matrix of rotation
        self._rotation_matrix_xyz_inv = np.eye(4, 4)  # inverse of matrix of rotation

        self.clear_colors: List[Tuple[float, float, float, float]] = list(self.CLEAR_COLORS)
        if clear_color not in self.clear_colors:
            self.clear_colors.append(clear_color)
        self.clear_color_index: int = -1

        self.label_type: int = 0  # 0 - no labels

    @property
    def perspective_angle(self) -> float:
        return self._perspective_angle

    @perspective_angle.setter
    def perspective_angle(self, angle: float):
        self._perspective_angle = angle

    @property
    def rotation_angles(self) -> np.ndarray:
        return self._rotation_angles

    @property
    def rotation_angles_radian(self) -> np.ndarray:
        return np.radians(self._rotation_angles)

    @rotation_angles.setter
    def rotation_angles(self, values: np.ndarray):
        self._rotation_angles = np.array(values)
        self._rotation_matrix_xyz = compute_rotation_matrix_xyz(self.rotation_angles_radian)
        self._rotation_matrix_xyz_inv = np.linalg.inv(self._rotation_matrix_xyz)

    @property
    def num_tex(self) -> int:
        return self.TEX_NUM

    @property
    def num_list(self) -> int:
        return self.NUM_LIST

    @property
    def clear_color(self) -> Tuple[float, float, float, float]:
        return self.clear_colors[self.clear_color_index]

    def switch_clear_color(self):
        self.clear_color_index = (self.clear_color_index + 1) % len(self.clear_colors)

    def switch_label_type(self):
        self.label_type = (self.label_type + 1) % 5
        self.ball_stick_model.set_atomic_labels_type(self.label_type)

    def switch_atoms_color_type(self):
        self.ball_stick_model.change_atomic_colors()
        if self.atoms_first_handle is not None:
            gl.glDeleteLists(self.atoms_first_handle, self.num_list)
            self.atoms_first_handle = None

        if self.species_first_handle is not None:
            gl.glDeleteLists(self.species_first_handle, self.ball_stick_model.num_atom_kinds)
            self.species_first_handle = None

        self.atoms_first_handle = gl.glGenLists(self.num_list)
        self.species_first_handle = gl.glGenLists(self.ball_stick_model.num_atom_kinds)
        self.model_initialisation()

    def initialize_gl(self):
        """ OpenGL initialisation """

        self.make_current_callback()
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glDepthMask(1)
        gl.glDepthFunc(gl.GL_LEQUAL)

    def paint_gl(self):
        """ Render the model. It is called automatically when the model needs to be redrawn
        (parent method update).
        Objects with an alpha channel should be drawn after opaque ones. All spheres with an alpha channel
        are drawn in the cycle without adding to the list. Semi-transparent spheres are always visible on
        one side. This is due to some imperfections in the rendering of spheres in the glusphere.
        """

        self.make_current_callback()
        try:
            self.prepare_scene()
            self.light_prepare()
            if self.is_active:
                self.prepare_orientation()
                gl.glCallList(self.atoms_first_handle + self.ATOMS)  # atoms
                gl.glCallList(self.atoms_first_handle + self.BONDS)  # bonds

                model = self.ball_stick_model

                if model.is_cell_visible:
                    gl.glCallList(self.atoms_first_handle + self.CELL)  # cell

                # Add texture for a constrained objects.
                for kind, pos in zip(model.atom_fixed2kind, model.atom_fixed2pos):
                    self.render_fix_atom_pattern(kind, pos)

                # Objects which need a rotation to the screen projection plane.
                for kind, pos in zip(model.atom_scr_rot2kind, model.atom_scr_rot2pos):
                    self.add_atom_screen_rotation(kind, pos)

                if self.is_labels_visible:
                    self.render_labels()

        except Exception as exc:
            print(exc)
            pass

    def apply_screen_rotation(self) -> None:
        """
        This method rotates a current gl primitive in such a manner that it looks towards
        the projection plane with the same side.
        Note: the rotation reverses the rotations in the self.prepare_orientation()
        """
        gl.glMultMatrixf(self._rotation_matrix_xyz_inv)

    def add_atom_screen_rotation(self, kind: int, pos: np.ndarray) -> None:
        """
        Add a primitive from the OpenGL list `self.species_first_handle + kind` after translating
        by `pos` and applying a screen rotation.
        Args:
            kind: pointer relative to self.species_first_handle storing the primitive
            pos: new position of the primitive `self.species_first_handle + kind`
        """
        gl.glPushMatrix()
        gl.glTranslated(*pos)
        self.apply_screen_rotation()
        gl.glCallList(self.species_first_handle + kind)
        gl.glPopMatrix()

    def add_atom(self, kind: int, pos: np.ndarray) -> None:
        """
        Add a primitive from the OpenGL list `self.species_first_handle + kind` after translating
        by a translation `pos`.
        Args:
            kind: pointer relative to self.species_first_handle storing the primitive
            pos: new position of the primitive `self.species_first_handle + kind`
        """
        gl.glPushMatrix()
        gl.glTranslated(*pos)
        gl.glCallList(self.species_first_handle + kind)
        gl.glPopMatrix()

    def render_fix_atom_pattern(self, kind: int, pos: np.ndarray) -> None:
        """
            Render constrain texture for atom
            Args:
            kind: pointer relative to kind of atom
            pos: constrained atom position
        """
        atomic_radius = self.ball_stick_model.kind2desc[kind].radius
        gl.glColor3f(*self.ball_stick_model.kind2desc[kind].contrast_color)

        delta = np.array([0.0, 0.0, -atomic_radius])
        delta = self.rotate_point(delta)

        gl.glPushMatrix()
        gl.glTranslated(*(pos - delta))
        self.apply_screen_rotation()
        gl.glRotated(45, 0, 0, 1)

        coord = self.ball_stick_model.kind2desc[kind].fixed_vert

        gl.glBegin(gl.GL_TRIANGLES)
        for point in coord:
            gl.glVertex2f(point[0], point[1])
        gl.glEnd()
        gl.glPopMatrix()

    def render_labels(self):
        """
        Render atomic labels
        """
        # Textured thing
        gl.glColor3f(0, 0, 0)
        gl.glEnable(gl.GL_TEXTURE_2D)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.tex_ids[OpenGLCore.TEX_FONT_ID])
        gl.glTexEnvf(gl.GL_TEXTURE_ENV, gl.GL_TEXTURE_ENV_MODE, gl.GL_MODULATE)
        rect_coord = [0, 0, 1, 0, 1, 1, 0, 1]
        rect_tex = [0] * 8
        char_size = 1 / 16.0
        char_width = 1.0
        for index, pos in enumerate(self.ball_stick_model.positions):
            text = self.ball_stick_model.atomic_labels[index]
            atomic_radius = self.ball_stick_model.get_radius(index)
            delta = np.array([0.1, 0.0, -atomic_radius])
            delta = self.rotate_point(delta)

            gl.glPushMatrix()
            gl.glTranslated(*(pos - delta))
            self.apply_screen_rotation()
            gl.glScalef(0.15, 0.15, 1)

            for let in text:
                rct_bottom, rct_left, rct_right, rct_top = self.symbol_coords_texture(
                    char_size, char_width, let)

                rect_tex[0] = rect_tex[6] = rct_left
                rect_tex[2] = rect_tex[4] = rct_right
                rect_tex[1] = rect_tex[3] = rct_bottom
                rect_tex[5] = rect_tex[7] = rct_top

                rect_coord[2] = rect_coord[4] = char_width

                gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
                gl.glEnableClientState(gl.GL_TEXTURE_COORD_ARRAY)

                gl.glVertexPointer(2, gl.GL_FLOAT, 0, rect_coord)
                gl.glTexCoordPointer(2, gl.GL_FLOAT, 0, rect_tex)

                gl.glDrawArrays(gl.GL_TRIANGLE_FAN, 0, 4)
                gl.glTranslated(char_width, 0, 0)

                gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
                gl.glDisableClientState(gl.GL_TEXTURE_COORD_ARRAY)

            gl.glPopMatrix()
        gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
        gl.glDisable(gl.GL_TEXTURE_2D)

    @staticmethod
    def symbol_coords_texture(char_size: float, char_width: float, let: str):
        """
        The texture coordinates of the two opposite corners of the square containing the symbol image
        Args:
            char_size: 1 / the number of characters in the texture line (float)
            char_width: fraction of the character's width in relation to the size of the square (now it is 1) ( float)
            let: desired character (str)
        Returns:
            True if atom labels are displayed
        """
        c = ord(let)     # ascii code
        y = c >> 4       # integer division by 16
        x = c & 0b1111   # remainder of the division
        rct_left = x * char_size
        rct_right = rct_left + char_size * char_width
        rct_top = y * char_size
        rct_bottom = rct_top + char_size
        return rct_bottom, rct_left, rct_right, rct_top

    @property
    def is_labels_visible(self) -> bool:
        """
        Criterion for displaying labels of atoms
        Returns:
            True if atom labels are displayed
        """
        return self.label_type > 0

    def rotate_point(self, point: np.ndarray) -> np.ndarray:
        """
        Rotate point in 3D space around point0.
        Args:
            point: point in 3D (np.ndarray).

        Returns:
            Point after rotation (np.ndarray).
        """
        return self._rotation_matrix_xyz[:3, :3].dot(point)

    def set_opengl_data(self, ball_stick_model: BallStickModel):
        """
        The function creates QuadObjS objects for atoms and bonds
        Args:
            ball_stick_model: BallAndStickModel - all information about model.
        """
        self.ball_stick_model = ball_stick_model
        self.label_type = 0
        self.make_current_callback()
        self.clear()
        self.atoms_first_handle = gl.glGenLists(self.num_list)
        self.species_first_handle = gl.glGenLists(self.ball_stick_model.num_atom_kinds)
        self.tex_ids[:] = gl.glGenTextures(self.TEX_NUM)
        self.fit_model_to_screen()
        self.tex_fill()
        self.model_initialisation()
        self.is_active = True

    def model_initialisation(self):
        self.init_species_quadrics()
        self.add_atoms()
        self.add_bonds()
        self.add_cell()

    def tex_fill(self):
        img_data = read_image('verdana_alpha.png')
        self.fill_texture_id(img_data, self.tex_ids[OpenGLCore.TEX_FONT_ID])

        self.is_tex_loaded = True

    def fit_model_to_screen(self):
        """
        Fit model to screen
        """
        atomic_numbers = self.ball_stick_model.atomic_numbers
        positions = self.ball_stick_model.positions

        self.perspective_angle = self.PERSPECTIVE_ANGLE
        x_max, y_max, z_max = positions.max(axis=0)
        rad = covalent_radii[atomic_numbers].max()
        h, w = self.height_callback(), self.width_callback()
        size = x_max + rad if h > w else y_max + rad
        dist = size / math.tan(math.radians(self.perspective_angle / 2))
        dist *= h / w
        y_dist = (y_max + rad) / math.tan(math.radians(self.perspective_angle / 2))
        dist = max(dist, y_dist)
        self.cam_position[:] = np.array([0, 0, z_max + dist])
        self.light0_position[2] = z_max + 10 * dist

    def clear(self):
        """ The function delete QuadObjS objects of atoms and bonds """
        self.is_active = False
        if self.atoms_first_handle is not None:
            gl.glDeleteLists(self.atoms_first_handle, self.num_list)
            self.atoms_first_handle = None

        if self.species_first_handle is not None:
            gl.glDeleteLists(self.species_first_handle, self.ball_stick_model.num_atom_kinds)
            self.species_first_handle = None

        if self.is_tex_loaded:
            gl.glDeleteTextures(self.TEX_NUM, self.tex_ids)
            self.tex_ids.fill(0)
            self.is_tex_loaded = False

    def init_species_quadrics(self):
        """
        Initialize and fill the OpenGL list corresponding to the atomic kinds. In simplest case,
        this list reciprocates the set of chemical species in the atomic system.
        """
        for i_kind, desc in enumerate(self.ball_stick_model.kind2desc):
            gl.glNewList(self.species_first_handle + i_kind, gl.GL_COMPILE)
            gl.glColor4f(*desc.color)
            qua = glu.gluNewQuadric()
            glu.gluSphere(qua, desc.radius, desc.num_phi, desc.num_theta)
            gl.glEndList()

    def add_atoms(self) -> None:
        """
        The function copies the objects from the species list.
        """

        gl.glNewList(self.atoms_first_handle + self.ATOMS, gl.GL_COMPILE)
        model = self.ball_stick_model
        for kind, pos in zip(model.atom_non_rot2kind, model.atom_non_rot2pos):
            self.add_atom(kind, pos)
        gl.glEndList()

    def add_bonds(self) -> None:
        """
        The function creates QuadObjS objects of bonds
        """

        radius = self.ball_stick_model.bond_radius
        begin_center_end = self.ball_stick_model.bonds_positions
        colors = self.ball_stick_model.bonds_colors
        gl.glNewList(self.atoms_first_handle + self.BONDS, gl.GL_COMPILE)
        for pos1, center, pos2, color1, color2 in zip(*begin_center_end, *colors):
            self.add_bond(pos1, center, pos2, color1, color2, radius)
        gl.glEndList()

    @staticmethod
    def add_bond(pos1: np.ndarray, center: np.ndarray, pos2: np.ndarray,
                 color1: np.ndarray, color2: np.ndarray, radius: float) -> None:
        """
        The function creates QuadObjS objects for a single bond between atoms in pos1 and pos2
        Args:
            pos1: coordinates of first atom.
            center: coordinates of the bond's midpoint.
            pos2: coordinates of second atom.
            color1: RGB color (each component varies in the range 0.0 ... 1.0)
            color2: RGB color ...
            radius: radius of the bond
        """
        rel1 = center - pos1
        half_len1 = np.linalg.norm(rel1)
        rotation_matrix = get_rotation_matrix_sph(rel1)

        rel2 = pos2 - center
        half_len2 = np.linalg.norm(rel2)

        gl.glColor4f(*color1)
        gl.glPushMatrix()
        transform_matrix = np.copy(rotation_matrix)
        transform_matrix[3, 0:3] = pos1
        gl.glMultMatrixf(transform_matrix)
        glu.gluCylinder(glu.gluNewQuadric(), radius, radius, half_len1, 15, 1)
        gl.glPopMatrix()

        gl.glColor4f(*color2)
        gl.glPushMatrix()
        transform_matrix = np.copy(rotation_matrix)
        transform_matrix[3, 0:3] = center
        gl.glMultMatrixf(transform_matrix)
        glu.gluCylinder(glu.gluNewQuadric(), radius, radius, half_len2, 15, 1)
        gl.glPopMatrix()

    def add_cell(self) -> None:
        radius = 0.02
        cell = self.ball_stick_model.cell
        point1 = - (cell[0] + cell[1] + cell[2]) / 2
        point2 = point1 + cell[0]
        point3 = point1 + cell[1]
        point4 = point2 + cell[1]
        point5 = point1 + cell[2]
        point6 = point2 + cell[2]
        point7 = point3 + cell[2]
        point8 = point4 + cell[2]

        gl.glNewList(self.atoms_first_handle + 2, gl.GL_COMPILE)
        gl.glColor3f(0, 0, 0)
        self.add_dash_bond(point1, point2, radius)
        self.add_dash_bond(point1, point3, radius)
        self.add_dash_bond(point1, point5, radius)
        self.add_dash_bond(point2, point4, radius)
        self.add_dash_bond(point2, point6, radius)
        self.add_dash_bond(point3, point4, radius)
        self.add_dash_bond(point3, point7, radius)
        self.add_dash_bond(point4, point8, radius)
        self.add_dash_bond(point5, point6, radius)
        self.add_dash_bond(point5, point7, radius)
        self.add_dash_bond(point6, point8, radius)
        self.add_dash_bond(point7, point8, radius)
        gl.glEndList()

    @staticmethod
    def add_dash_bond(pos1: np.ndarray, pos2: np.ndarray, radius=0.1):
        """
        The function creates QuadObjS objects for a single dash line between pos1 and pos2
        Args:
            pos1: coordinates of first point (np.ndarray).
            pos2: coordinates of second point (np.ndarray).
            radius: radius of the bond
        """
        rel = pos2 - pos1
        n = 10
        binding_len = np.linalg.norm(rel)   # cylinder height
        if binding_len != 0:
            fall = 180.0 / math.pi * math.acos(rel[2] / binding_len)
            yaw = 180.0 / math.pi * math.atan2(rel[1], rel[0])

            for i in range(n):
                gl.glPushMatrix()
                gl.glTranslated(*(pos1 + i * rel / (1.0 * n - 0.5)))
                gl.glRotated(yaw, 0, 0, 1)
                gl.glRotated(fall, 0, 1, 0)
                glu.gluCylinder(glu.gluNewQuadric(),
                                radius,  # /*baseRadius:*/
                                radius,  # /*topRadius:*/
                                binding_len / (2.0 * n),  # /*height:*/
                                15,  # /*slices:*/
                                1)  # /*stacks:*/
                gl.glPopMatrix()

    @staticmethod
    def fill_texture_id(img_data: np.ndarray, texture_id: int):
        """
        Configures a texture with the specified identifier.
        Args:
            img_data: bitmap data for texture (np.ndarray)
            texture_id: identifier of texture (int)
        """
        gl.glBindTexture(gl.GL_TEXTURE_2D, texture_id)
        gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_REPEAT)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_REPEAT)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glTexParameterf(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)
        img_size0, img_size1, m = img_data.shape
        assert m == 4
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, img_size0, img_size1, 0,
                        gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, img_data)

    def light_prepare(self):
        """ Light configuration """
        # clear the COLOR and DEPTH buffers
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)

        # material properties
        material_diffuse = (1.0, 1.0, 1.0, 1.0)
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_DIFFUSE, material_diffuse)
        gl.glEnable(gl.GL_LIGHTING)

        ambient = (0.2, 0.2, 0.2, 1)
        gl.glLightModelfv(gl.GL_LIGHT_MODEL_AMBIENT, ambient)
        # Determine the current lighting model
        gl.glLightModelf(gl.GL_LIGHT_MODEL_TWO_SIDE, gl.GL_TRUE)  # two-side lighting calculation

        gl.glEnable(gl.GL_LIGHT0)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_COLOR_MATERIAL)

        # Determine the position of the light source
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, self.light0_position)

    def set_rotations_angle(self, rotations_angle):
        """
        Set angles of rotation.
        Args:
            rotations_angle: angles of rotation (np.ndarray).

        Returns:
            None.
        """
        self.rotation_angles = rotations_angle

    def prepare_orientation(self):
        """ The function transforms matrix of model """
        gl.glTranslated(*(-self.cam_position))
        gl.glMultMatrixf(self._rotation_matrix_xyz)

    def prepare_scene(self):
        """ OpenGL configuration """
        gl.glClearColor(*self.clear_color)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        x, y, width, height = gl.glGetDoublev(gl.GL_VIEWPORT)
        glu.gluPerspective(
                self.perspective_angle,  # field of view in degrees
                width / float(height or 1),  # aspect ratio
                0.25,  # near clipping plane
                500)  # far clipping plane
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

    def rotate(self, xs: int, ys: int, width: int, height: int) -> bool:
        """
        Rotate the model
        Args:
            xs: current horizontal position of the mouse cursor relative to the OpenGL widget.
            ys: current vertical position of the mouse cursor relative to the OpenGL widget.
            width: horizontal size of the OpenGL widget
            height: vertical size of the OpenGL widget

        Returns:
            True if there has been a rotation (and we need to redraw) or False otherwise.
        """
        is_rotated = False
        if self.is_active:
            if (xs - self.xs_old != 0) or (ys - self.ys_old != 0):
                rotations_angle = self.rotation_angles
                rotations_angle[1] -= 180.0 / width * (xs - self.xs_old)
                rotations_angle[0] -= 180.0 / height * (ys - self.ys_old)
                self.rotation_angles = rotations_angle
                self.xs_old, self.ys_old = xs, ys
                is_rotated = True
        return is_rotated

    def pan(self, xs: int, ys: int, width: int, height: int) -> bool:
        """
        Pan operation
        Args:
            xs: current horizontal position of the mouse cursor relative to the OpenGL widget.
            ys: current vertical position of the mouse cursor relative to the OpenGL widget.
            width: horizontal size of the OpenGL widget
            height: vertical size of the OpenGL widget

        Returns:
            True if the camera position has been changed (and we need to redraw), False otherwise.
        """
        is_cam_pos_changed = False
        if self.is_active:
            if (xs - self.xs_old != 0) or (ys - self.ys_old != 0):
                self.cam_position[1] += 5.0 / height * (ys - self.ys_old)
                self.cam_position[0] -= 5.0 / width * (xs - self.xs_old)
                self.xs_old, self.ys_old = xs, ys
                is_cam_pos_changed = True
        return is_cam_pos_changed

    def set_cursor_coord(self, x: int, y: int) -> bool:
        """
        Remember current cursor position
        Args:
            x: horizontal position of the mouse cursor relative to the OpenGL widget.
            y: vertical position of the mouse cursor relative to the OpenGL widget.

        Returns:
            True if cursor position in Screen coordinates has been a changed or False otherwise.
        """
        if self.is_active:
            xs_old, ys_old = x, y
            if self.xs_old != xs_old and self.ys_old != ys_old:
                self.xs_old, self.ys_old = xs_old, ys_old
                return True
        return False

    def scale(self, angle):
        """
        This function changes the perspective matrix parameter. It affects the scaling of the model
        Args:
            angle: relative amount that the wheel was rotated, in eighths of a degree.

        Returns:
            True if there has been a scaling (and we need to redraw) or False otherwise.
        """
        is_scaled = False
        if self.is_active:
            self.cam_position[2] += angle / 1200.0
            is_scaled = True
        return is_scaled
