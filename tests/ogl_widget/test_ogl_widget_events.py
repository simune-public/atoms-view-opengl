from qtpy.QtCore import Qt


class MockAngleDelta:
    @staticmethod
    def y():
        return 10


class MockEvent:

    def __init__(self, _button):
        self.button = _button

    @staticmethod
    def angleDelta() -> MockAngleDelta:
        return MockAngleDelta()

    def buttons(self):
        return self.button

    @staticmethod
    def x():
        return 11

    @staticmethod
    def y():
        return 12


def test_events_key_release_a_b(ogl_widget_water_atoms, qtbot):
    widget = ogl_widget_water_atoms
    qtbot.keyRelease(widget, 'A')
    assert widget.gl_core.clear_color_index == -1
    qtbot.keyRelease(widget, 'B')
    assert widget.gl_core.clear_color_index == 0


def test_events_key_release_l(ogl_widget_water_atoms, qtbot):
    widget = ogl_widget_water_atoms
    assert widget.gl_core.label_type == 0
    qtbot.keyRelease(widget, 'L')
    assert widget.gl_core.label_type == 1


def test_events_key_release_c(ogl_widget_water_atoms, qtbot):
    widget = ogl_widget_water_atoms
    assert widget.gl_core.ball_stick_model.kind2desc[widget.gl_core.ball_stick_model.atom2kind[0]].color_type == 0
    qtbot.keyRelease(widget, 'C')
    assert widget.gl_core.ball_stick_model.kind2desc[widget.gl_core.ball_stick_model.atom2kind[0]].color_type == 1


def test_events_wheel(ogl_widget_water_atoms, mocker):
    widget = ogl_widget_water_atoms
    event = MockEvent(Qt.RightButton)

    mock_update = mocker.patch.object(widget, 'update')
    widget.wheelEvent(event)
    assert mock_update.isCalledOnce()


def test_events_mouse(ogl_widget_water_atoms, mocker):
    widget = ogl_widget_water_atoms
    event = MockEvent(Qt.RightButton)

    mock_update = mocker.patch.object(widget, 'update')
    widget.mouseMoveEvent(event)
    assert mock_update.isCalledOnce()

    widget.mousePressEvent(event)
    assert widget.gl_core.xs_old == event.x()
    assert widget.gl_core.ys_old == event.y()

    event = MockEvent(Qt.LeftButton)
    widget.gl_core.xs_old = 1.1 * event.x()
    widget.mouseMoveEvent(event)
    assert mock_update.isCalledOnce()
