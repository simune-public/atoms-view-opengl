
from typing import Callable
from pathlib import Path

import pytest

from ase.build import molecule, bulk
from ase import Atoms
from ase.constraints import FixAtoms

from atoms_view_opengl.oglwidget import OGLWidget
from atoms_view_opengl.ball_stick_model import BallStickModel


@pytest.fixture
def tests_path() -> Path:
    return Path(__file__).parent


@pytest.fixture
def water_atoms() -> Atoms:
    return molecule('H2O')


@pytest.fixture
def water_atoms_h1_constrained(water_atoms) -> Atoms:
    water_atoms.set_constraint(FixAtoms((1,)))
    return water_atoms


@pytest.fixture
def water_atoms_h1_ghost(water_atoms) -> Atoms:
    water_atoms.set_tags((0, -1, 0))
    return water_atoms


@pytest.fixture
def silver_atoms() -> Atoms:
    return bulk('Ag')


@pytest.fixture
def get_ball_and_stick_model() -> Callable[[Atoms], BallStickModel]:

    def factory_function(atoms) -> BallStickModel:
        return BallStickModel(atoms)

    return factory_function


@pytest.fixture
def ball_stick_model(water_atoms) -> BallStickModel:
    return BallStickModel(water_atoms)


@pytest.fixture
def get_ogl_widget(qapp) -> Callable[[Atoms], OGLWidget]:

    def factory_function(atoms) -> BallStickModel:
        widget = OGLWidget()
        widget.show()
        widget.plot(atoms)
        return widget

    return factory_function


@pytest.fixture
def ogl_widget_water_atoms(water_atoms, get_ogl_widget) -> OGLWidget:
    return get_ogl_widget(water_atoms)


@pytest.fixture
def ogl_widget_plot_before_show(qapp, water_atoms) -> OGLWidget:
    widget = OGLWidget()
    widget.plot(water_atoms)
    return widget

